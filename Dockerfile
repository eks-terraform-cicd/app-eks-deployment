FROM nginx:1.19.0-alpine

ARG BRANCH_NAME=main

COPY index.html /usr/share/nginx/html/

RUN sed -i "s/BRANCH_NAME/$BRANCH_NAME/g" /usr/share/nginx/html/index.html
